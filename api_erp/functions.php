<?php
/**
 * Created by PhpStorm.
 * User: diego
 * Date: 27/04/17
 * Time: 15:23
 */
function hash_encode($char)
{
    return base64_encode(base64_encode(base64_encode($char . session_id())));
}
function hash_decode($hash)
{
    return str_replace(session_id(),'',(base64_decode(base64_decode(base64_decode($hash)))));
}
function response($response) {
    switch (gettype($response)){
        case 'array':
            return json_encode($response);
            break;
        case 'object':
            return json_encode((array) $response);
            break;
        case 'boolean':
            return $response ? 'true' : 'false';
            break;
        default:
            return (string) $response;
            break;
    }
}
