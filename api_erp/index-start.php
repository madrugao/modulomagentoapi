<?php

require_once(__DIR__.'/../app/Mage.php'); //Path to Magento

Mage::app();

require_once __DIR__.'/vendor/autoload.php';


$app = new Silex\Application();

$app['debug'] = true;

include_once(__DIR__.'/functions.php');
include_once(__DIR__.'/routes/web.php');

$app->run();