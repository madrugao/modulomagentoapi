<?php

require_once(__DIR__.'/../app/Mage.php'); //Path to Magento

Mage::app()->setCurrentStore(Mage::getModel('core/store')->load(Mage_Core_Model_App::ADMIN_STORE_ID));
$installer = new Mage_Sales_Model_Mysql4_Setup;
$attribute  = array(
    'type'          => 'timestamp',
    'backend_type'  => 'timestamp',
    'frontend_input' => 'text',
    'is_user_defined' => false,
    'label'         => '',
    'visible'       => false,
    'required'      => false,
    'user_defined'  => false,
    'searchable'    => false,
    'filterable'    => false,
    'comparable'    => false,
    'default'       => ''
);
$installer->addAttribute('order', 'mapy_reported_at', $attribute);
$installer->addAttribute('order', 'mapy_downloaded_at', $attribute);
$installer->addAttribute('catalog_product', 'mapy_downloaded_at', $attribute);
$installer->endSetup();

if(!file_exists('index-setup.php')){
    rename('index.php', 'index-setup.php');
    rename('index-start.php', 'index.php');
}

require_once __DIR__.'/vendor/autoload.php';

$app = new Silex\Application();

$app['debug'] = true;

include_once(__DIR__.'/functions.php');
include_once(__DIR__.'/routes/web.php');

$app->run();