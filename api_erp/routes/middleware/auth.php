<?php

return function (\Symfony\Component\HttpFoundation\Request $request)
{
    session_start();
    $model = new Mage_Api_Model_User();
    $user = $model->load(hash_decode($request->get('token')));
    if(!$user->getId())
        return new \Symfony\Component\HttpFoundation\Response(response(['error'=>'login invalido']));
};