<?php

use Symfony\Component\HttpFoundation\Request;

foreach (glob('{'.__DIR__.'/web/*.php,'.__DIR__.'/web/*/*.php,'.__DIR__.'/web/*/*/*.php,'.__DIR__.'/web/*/*/*/*.php,'.__DIR__.'/web/*/*/*/*/*.php,'.__DIR__.'/web/*/*/*/*/*/*.php}',GLOB_BRACE) as $filename)
{
    include $filename;
}

// get Middleware
$auth = require(__DIR__.'/middleware/auth.php');
$app->get('/', function(){
    return 'v0.0.1';
});
//get rotas
$app->post('/login', function(Request $request){
    session_start();
    $model = new Mage_Api_Model_User();
    $user = $model->login($request->get('username'), $request->get('password'));
    if(!$user->getId())
        return response([
            'error' => 'Usuario e/ou senha invalido!',
        ]);
    return response([
        'hash' => hash_encode($user->getId())
    ]);
});