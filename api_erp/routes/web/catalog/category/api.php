 <?php
/**
 * Created by PhpStorm.
 * User: heisenberg
 * Date: 5/1/17
 * Time: 3:06 PM
 */

use Symfony\Component\HttpFoundation\Request;

 $app->get('/catalog/category/level/{categoryId}', function (Request $request, $categoryId) {
     $website = $request->get('website');
     $store = $request->get('store');
     return response((new Mapy_Model_Catalog_Category_Api_V2())->level($website, $store, $categoryId));
 })->before($auth);

 $app->get('/catalog/category/tree/{parentId}/store/{store}', function ($parentId, $store) {
     return response((new Mapy_Model_Catalog_Category_Api_V2())->tree($parentId, $store));
 })->before($auth)->value('store',null);

 $app->get('/catalog/category/init_category/{categoryId}', function (Request $request, $categoryId) {
     $store = $request->get('store');
     return response((new Mapy_Model_Catalog_Category_Api_V2())->_initCategory($categoryId, $store));
 })->before($auth);

 $app->get('/catalog/category/info/{categoryId}/store/{store}', function (Request $request, $categoryId, $store) {
     $attributes = json_decode($request->get('attributes'), 1);
     return response((new Mapy_Model_Catalog_Category_Api_V2())->info($categoryId, $store, $attributes));
 })->before($auth)->value('store',null);

 $app->post('/catalog/category/create/{parentId}/{store}', function (Request $request, $parentId, $store) {
     $categoryData = json_decode($request->get('categoryData'), 1);
     return response((new Mapy_Model_Catalog_Category_Api_V2())->create($parentId, $store, $categoryData));
 })->before($auth)->value('store',null);

 $app->put('/catalog/category/update/{categoryId}/{store}', function (Request $request, $categoryId, $store) {
     $categoryData = json_decode($request->get('categoryData'), 1);
     return response((new Mapy_Model_Catalog_Category_Api_V2())->update($categoryId, $store, $categoryData));
 })->before($auth)->value('store',null);

 $app->put('/catalog/category/move/{categoryId}/{parentId}/{afterId}', function ($categoryId, $parentId, $afterId) {
     return response((new Mapy_Model_Catalog_Category_Api_V2())->move($categoryId, $parentId, $afterId));
 })->before($auth)->value('afterId',null);

 $app->delete('/catalog/category/delete/{categoryId}', function ($categoryId) {
     return response((new Mapy_Model_Catalog_Category_Api_V2())->delete($categoryId));
 })->before($auth);

 $app->get('/catalog/category/get_product_id/{categoryId}/{store}', function ($categoryId, $store ) {
     return response((new Mapy_Model_Catalog_Category_Api_V2())->_getProductId($categoryId, $store));
 })->before($auth)->value('store',null);

 $app->get('/catalog/category/assigned_products/{categoryId}/{store}', function ($categoryId, $store){
     return response((new Mapy_Model_Catalog_Category_Api_V2())->assignedProducts($categoryId, $store));
 })->before($auth)->value('store',null);

 $app->get('/catalog/category/assign_product/{categoryId}/{productId}/{position}/{identifierType}', function ($categoryId, $productId, $position, $identifierType){
     return response((new Mapy_Model_Catalog_Category_Api_V2())->assignProduct($categoryId, $productId, $position, $identifierType));
 })->before($auth)->value('position',null)->value('identifierType',null);

 $app->put('/catalog/category/update_product/{categoryId}/{productId}/{position}/{identifierType}', function ($categoryId, $productId, $position, $identifierType){
     return response((new Mapy_Model_Catalog_Category_Api_V2())->updateProduct($categoryId, $productId, $position, $identifierType));
 })->before($auth)->value('position',null)->value('identifierType',null);

 $app->delete('/catalog/category/remove_product/{categoryId}/{productId}/{identifierType}', function ($categoryId, $productId, $identifierType){
     return response((new Mapy_Model_Catalog_Category_Api_V2())->removeProduct($categoryId, $productId, $identifierType));
 })->before($auth)->value('identifierType',null);