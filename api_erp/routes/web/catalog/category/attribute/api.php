<?php
/**
 * Created by PhpStorm.
 * User: heisenberg
 * Date: 5/1/17
 * Time: 3:09 PM
 */

$app->get('/catalog/category/attribute/items', function () {
    return response((new Mapy_Model_Catalog_Category_Attribute_Api_V2())->items());
})->before($auth);

$app->get('/catalog/category/attribute/options/{attributeId}/{store}', function ($attributeId, $store) {
    return response((new Mapy_Model_Catalog_Category_Attribute_Api_V2())->options($attributeId, $store));
})->before($auth)->value('store',null);
