<?php
/**
 * Created by PhpStorm.
 * User: heisenberg
 * Date: 5/1/17
 * Time: 4:14 PM
 */

$app->get('/catalog/category/indexer/flat/is_visible', function () {
    return response((new Mage_Catalog_Model_Category_Indexer_Flat())->isVisible());
})->before($auth);

$app->get('/catalog/category/indexer/flat/get_name', function () {
    return response((new Mage_Catalog_Model_Category_Indexer_Flat())->getName());
})->before($auth);

$app->get('/catalog/category/indexer/flat/get_description', function () {
    return response((new Mage_Catalog_Model_Category_Indexer_Flat())->getDescription());
})->before($auth);

$app->get('/catalog/category/indexer/flat/get_indexer', function () {
    return response((new Mage_Catalog_Model_Category_Indexer_Flat())->_getIndexer());
})->before($auth);

$app->get('/catalog/category/indexer/flat/reindex_all', function () {
    return response((new Mage_Catalog_Model_Category_Indexer_Flat())->reindexAll());
})->before($auth);