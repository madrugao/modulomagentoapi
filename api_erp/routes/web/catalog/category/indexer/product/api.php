<?php
/**
 * Created by PhpStorm.
 * User: heisenberg
 * Date: 5/1/17
 * Time: 3:09 PM
 */

$app->get('/catalog/category/indexer/product/get_name', function () {
    return response((new Mage_Catalog_Model_Category_Indexer_Product())->getName());
})->before($auth);

$app->get('/catalog/category/indexer/product/get_description', function () {
    return response((new Mage_Catalog_Model_Category_Indexer_Product())->getDescription());
})->before($auth);