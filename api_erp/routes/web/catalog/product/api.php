<?php
/**
 * Created by PhpStorm.
 * User: heisenberg
 * Date: 5/1/17
 * Time: 8:24 PM
 */

use Symfony\Component\HttpFoundation\Request;

$app->get('/catalog/product/items/new/{page}/{store}', function (Request $request, $page, $store) {
    $filters = json_decode($request->get('filters'), 1);
    return response((new Mapy_Model_Catalog_Product_Api_V2())->itemsNews($page, $filters, $store));
})->before($auth)->value('store',null)->value('page', 1);

$app->get('/catalog/product/items/{page}/{store}', function (Request $request, $page, $store) {
    $filters = json_decode($request->get('filters'), 1);
    return response((new Mapy_Model_Catalog_Product_Api_V2())->items($page, $filters, $store));
})->before($auth)->value('store',null)->value('page', 1);

$app->get('/catalog/product/info/download/{productId}/{store}/{identifierType}', function (Request $request, $productId, $store, $identifierType) {
    $attributes = json_decode($request->get('attributes'), 1);
    return response((new Mapy_Model_Catalog_Product_Api_V2())->infoDownload($productId, $store, $attributes, $identifierType));
})->before($auth)->value('store', null)->value('identifierType',null);

$app->get('/catalog/product/info/{productId}/{store}/{identifierType}', function (Request $request, $productId, $store, $identifierType) {
    $attributes = json_decode($request->get('attributes'), 1);
    return response((new Mapy_Model_Catalog_Product_Api_V2())->info($productId, $store, $attributes, $identifierType));
})->before($auth)->value('store', null)->value('identifierType',null);

$app->post('/catalog/product/create/{type}/{set}/{sku}/{store}', function(Request $request, $type, $set, $sku, $store) {
    $data = json_decode($request->get('data'), 1);
    return response((new Mapy_Model_Catalog_Product_Api_V2())->create($type, $set, $sku, $data, $store));
})->before($auth)->value('store',null);

$app->put('/catalog/product/update/{productId}/{store}/{identifierType}', function(Request $request, $productId, $store, $identifierType) {
    $data = json_decode($request->get('data'), 1);
    return response((new Mapy_Model_Catalog_Product_Api_V2())->update($productId, $data, $store, $identifierType));
})->before($auth)->value('store',null)->value('identifierType',null);

$app->post('/catalog/product/setSpecialPrice/{productId}/{specialPrice}/{fromDate}/{toDate}/{store}', function($productId, $specialPrice , $fromDate, $toDate, $store) {
    return response((new Mapy_Model_Catalog_Product_Api_V2())->setSpecialPrice($productId, $specialPrice, $fromDate, $toDate, $store));
})->before($auth)->value('store',null);

$app->get('/catalog/product/get_special_price/{productId}/{store}', function ($productId, $store) {
    return response((new Mapy_Model_Catalog_Product_Api_V2())->getSpecialPrice($productId, $store));
})->before($auth)->value('store',null);

$app->delete('/catalog/product/delete/{productId}/{identifierType}', function ($productId, $identifierType) {
    return response((new Mapy_Model_Catalog_Product_Api_V2())->delete($productId, $identifierType));
})->before($auth)->value('identifierType',null);

$app->get('/catalog/product/get_additional_attributes/{productType}/{attributeSetId}', function($productType, $attributeSetId) {
    return response((new Mapy_Model_Catalog_Product_Api_V2())->getAdditionalAttributes($productType, $attributeSetId));
})->before($auth);