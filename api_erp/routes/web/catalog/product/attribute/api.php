<?php
/**
 * Created by PhpStorm.
 * User: heisenberg
 * Date: 5/1/17
 * Time: 8:24 PM
 */
use Symfony\Component\HttpFoundation\Request;

$app->get('/catalog/product/attribute/items/{setId}', function ($setId) {
    return response((new Mapy_Model_Catalog_Product_Attribute_Api_V2())->items($setId));
})->before($auth);

$app->get('/catalog/product/attribute/options/{attributeId}/{store}', function ($attributeId, $store) {
    return response((new Mapy_Model_Catalog_Product_Attribute_Api_V2())->options($attributeId, $store));
})->before($auth)->value('store',null);

$app->post('/catalog/product/attribute/create', function (Request $request) {
    $data = json_decode($request->get('data'), 1);
    return response((new Mapy_Model_Catalog_Product_Attribute_Api_V2())->create($data));
})->before($auth);

$app->post('/catalog/product/attribute/update/{attributeId}', function (Request $request, $attributeId) {
    $data = json_decode($request->get('data'), 1);
    return response((new Mapy_Model_Catalog_Product_Attribute_Api_V2())->update($attributeId, $data));
})->before($auth);

$app->post('/catalog/product/attribute/remove/{attributeId}', function ($attributeId) {
    return response((new Mapy_Model_Catalog_Product_Attribute_Api_V2())->remove($attributeId));
})->before($auth);

$app->get('/catalog/product/attribute/info/{attributeId}', function ($attributeId) {
    return response((new Mapy_Model_Catalog_Product_Attribute_Api_V2())->info($attributeId));
})->before($auth);

$app->get('/catalog/product/attribute/add_option/{attributeId}', function (Request $request, $attributeId) {
    $data = json_decode($request->get('data'), 1);
    return response((new Mapy_Model_Catalog_Product_Attribute_Api_V2())->addOption($attributeId, $data));
})->before($auth);

$app->get('/catalog/product/attribute/remove_option/{attributeId}/{optionId}', function ($attributeId, $optionId) {
    return response((new Mapy_Model_Catalog_Product_Attribute_Api_V2())->removeOption($attributeId, $optionId));
})->before($auth);

