<?php
/**
 * Created by PhpStorm.
 * User: heisenberg
 * Date: 5/1/17
 * Time: 8:24 PM
 */

use Symfony\Component\HttpFoundation\Request;

$app->get('/catalog/product/link/items/{productId}/{type}/{identifierType}', function ($productId, $type, $identifierType) {
    return response((new Mapy_Model_Catalog_Product_Link_Api_V2())->items($type, $productId, $identifierType));
})->before($auth)->value('identifierType',null);

$app->get('/catalog/product/link/assign/{productId}/{type}/{linkedProductId}/{identifierType}', function (Request $request, $productId, $type, $linkedProductId, $identifierType) {
    $data = json_decode($request->get('data'), 1);
    return response((new Mapy_Model_Catalog_Product_Link_Api_V2())->assign($type, $productId, $linkedProductId, $data, $identifierType));
})->before($auth)->value('identifierType',null);

$app->put('/catalog/product/link/update/{productId}/{type}/{linkedProductId}/{identifierType}', function (Request $request, $type, $productId, $linkedProductId, $identifierType) {
    $data = json_decode($request->get('data'), 1);
    return response((new Mapy_Model_Catalog_Product_Link_Api_V2())->update($type, $productId, $linkedProductId, $data, $identifierType));
})->before($auth)->value('identifierType',null);

$app->delete('/catalog/product/link/remove/{productId}/{type}/{linkedProductId}/{identifierType}', function ($productId, $type, $linkedProductId, $identifierType) {
    return response((new Mapy_Model_Catalog_Product_Link_Api_V2())->remove($type, $productId, $linkedProductId, $identifierType));
})->before($auth)->value('identifierType',null);

$app->get('/catalog/product/link/attributes/{type}', function ($type) {
    return response((new Mapy_Model_Catalog_Product_Link_Api_V2())->attributes($type));
})->before($auth);

$app->get('/catalog/product/link/types', function () {
    return response((new Mapy_Model_Catalog_Product_Link_Api_V2())->types());
})->before($auth);

$app->get('/catalog/product/link/types', function () {
    return response((new Mapy_Model_Catalog_Product_Link_Api_V2())->types());
})->before($auth);