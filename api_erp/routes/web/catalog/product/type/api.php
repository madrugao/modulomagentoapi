<?php
/**
 * Created by PhpStorm.
 * User: heisenberg
 * Date: 5/1/17
 * Time: 8:24 PM
 */

$app->get('/catalog/product/type/items', function () {
    return response((new Mapy_Model_Catalog_Product_Type_Api_V2())->items());
})->before($auth);