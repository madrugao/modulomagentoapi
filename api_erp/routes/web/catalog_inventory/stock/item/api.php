<?php
/**
 * Created by PhpStorm.
 * User: heisenberg
 * Date: 5/3/17
 * Time: 7:51 PM
 */

use Symfony\Component\HttpFoundation\Request;

$app->get('/catalog_inventory/stock/items/', function (Request $request) {
    $productIds = json_decode($request->get('productIds'), 1);
    return response((new Mapy_Model_CatalogInventory_Stock_Item_Api_V2())->items($productIds));
})->before($auth);

$app->put('/catalog_inventory/stock/item/update/{productId}', function (Request $request, $productId) {
    $data = $request->get('data');
    return response((new Mapy_Model_CatalogInventory_Stock_Item_Api_V2())->update($productId, $data));
})->before($auth);

$app->put('/catalog_inventory/stock/item/multiUpdate', function (Request $request) {
    $productIds = $request->get('productIds');
    $data = $request->get('data');
    return response((new Mapy_Model_CatalogInventory_Stock_Item_Api_V2())->multiUpdate($productIds, $data));
})->before($auth);