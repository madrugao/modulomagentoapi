<?php
/**
 * Created by PhpStorm.
 * User: heisenberg
 * Date: 4/29/17
 * Time: 6:14 PM
 */

use Symfony\Component\HttpFoundation\Request;

$app->post('/mapy/cms/block/create', function (Request $request) {
    $data = json_decode($request->get('data'), 1);
    return response((new Mapy_Model_Mapy_Cms_Block_Api())->create($data));
})->before($auth);

$app->put('/mapy/cms/block/update/{blockId}', function (Request $request, $blockId) {
    $data = json_decode($request->get('data'), 1);
    return response((new Mapy_Model_Mapy_Cms_Block_Api())->update($blockId, $data));
})->before($auth);

$app->get('/mapy/cms/block/get_collection', function (Request $request) {
    $removeContent = $request->get('removeContent');
    return response((new Mapy_Model_Mapy_Cms_Block_Api())->getCollection($removeContent));
})->before($auth);

$app->delete('/mapy/cms/block/delete/{blockId}', function ($blockId) {
    return response((new Mapy_Model_Mapy_Cms_Block_Api())->delete($blockId));
})->before($auth);

$app->get('/mapy/cms/block/info/{blockId}', function ($blockId) {
    return response((new Mapy_Model_Mapy_Cms_Block_Api())->info($blockId));
})->before($auth);