<?php
/**
 * Created by PhpStorm.
 * User: heisenberg
 * Date: 4/29/17
 * Time: 6:14 PM
 */

use Symfony\Component\HttpFoundation\Request;

$app->post('/mapy/cms/page/create', function (Request $request) {
    $data = json_decode($request->get('data'), 1);
    return response((new Mapy_Model_Mapy_Cms_Page_Api())->create($data));
})->before($auth);

$app->put('/mapy/cms/page/update/{pageId}', function (Request $request, $pageId) {
    $data = json_decode($request->get('data'), 1);
    return response((new Mapy_Model_Mapy_Cms_Page_Api())->update($pageId, $data));
})->before($auth);

$app->get('/mapy/cms/page/get_collection/{removeContent}', function (Request $request) {
    $removeContent = $request->get('removeContent');
    return response((new Mapy_Model_Mapy_Cms_Page_Api())->getCollection($removeContent));
})->before($auth);

$app->delete('/mapy/cms/page/delete/{pageId}', function ($pageId) {
    return response((new Mapy_Model_Mapy_Cms_Page_Api())->delete($pageId));
})->before($auth);

$app->get('/mapy/cms/page/info/{pageId}', function ($pageId) {
    return response((new Mapy_Model_Mapy_Cms_Page_Api())->info($pageId));
})->before($auth);