<?php
/**
 * Created by PhpStorm.
 * User: heisenberg
 * Date: 4/29/17
 * Time: 6:37 PM
 */

use Symfony\Component\HttpFoundation\Request;

$app->get('/mapy/core/current_version/{asType}', function ($asType = '') {
    return response((new Mapy_Model_Mapy_Core_Api())->currentVersion($asType));
})->before($auth);

$app->get('/mapy/core/current_zend_version', function () {
    return response((new Mapy_Model_Mapy_Core_Api())->currentZendVersion());
})->before($auth);

$app->get('/mapy/core/get_url', function (Request $request) {
    $route = $request->get('route');
    $params = json_decode($request->get('params'), 1);
    return response((new Mapy_Model_Mapy_Core_Api())->getUrl($route, $params));
})->before($auth);

$app->get('/mapy/core/get_websites/{asType}', function ($asType) {
    return response((new Mapy_Model_Mapy_Core_Api())->getWebsites($asType));
})->before($auth)->value('asType','array');

$app->get('/mapy/core/get_stores/{asType}', function ($asType) {
    return response((new Mapy_Model_Mapy_Core_Api())->getStores($asType));
})->before($auth)->value('asType','array');

$app->get('/mapy/core/get_store_config/{store}', function (Request $request, $store) {
    $path = $request->get('path');
    return response((new Mapy_Model_Mapy_Core_Api())->getStoreConfig($path, $store));
})->before($auth)->value('store',null);

$app->get('/mapy/core/get_is_developer_mode', function(){
   return response((new Mapy_Model_Mapy_Core_Api())->getIsDeveloperMode());
})->before($auth);

$app->get('/mapy/core/log', function(Request $request){
    $message = $request->get('message');
    $level = $request->get('level');
    $file = $request->get('file');
    $forceLog = $request->get('forcelog');
    return response((new Mapy_Model_Mapy_Core_Api())->log($message, $level, $file, $forceLog));
})->before($auth);
