<?php
/**
 * Created by PhpStorm.
 * User: heisenberg
 * Date: 4/29/17
 * Time: 6:26 PM
 */

use Symfony\Component\HttpFoundation\Request;

$app->delete('/mapy/core/store/delete/{storeId}', function ($storeId) {
    return response((new Mapy_Model_Mapy_Core_Store_Api())->delete($storeId));
})->before($auth);

$app->get('/mapy/core/store/is_can_delete/{storeId}', function ($storeId) {
    return response((new Mapy_Model_Mapy_Core_Store_Api())->isCanDelete($storeId));
})->before($auth);

$app->put('/mapy/core/store/update/{storeId}', function (Request $request, $storeId) {
    $data = json_decode($request->get('data'), 1);
    return response((new Mapy_Model_Mapy_Core_Store_Api())->update($storeId, $data));
})->before($auth);

$app->post('/mapy/core/store/create', function (Request $request) {
    $data = json_decode($request->get('data'), 1);
    return response((new Mapy_Model_Mapy_Core_Store_Api())->create($data));
})->before($auth);

$app->get('/mapy/core/store/get_mage_core_model_store_as_array/{storeId}', function ($storeId) {
    return response((new Mapy_Model_Mapy_Core_Store_Api())->getMageCoreModelStoreAsArray($storeId));
})->before($auth);

$app->post('/mapy/core/store/format_price', function (Request $request, $storeId) {
    $data = json_decode($request->get('data'));
    return response((new Mapy_Model_Mapy_Core_Store_Api())->formatPrice($storeId, $data->price, $data->includeContainer));
})->before($auth);

$app->get('/mapy/core/store/get_config/{storeId}', function (Request $request, $storeId) {
    $path = $request->get('path');
    return response((new Mapy_Model_Mapy_Core_Store_Api())->getConfig($storeId, $path));
})->before($auth);

$app->get('/mapy/core/store/get_available_currency_codes/{storeId}', function (Request $request, $storeId) {
    $skipBaseNotAllowed = $request->get('skipBaseNotAllowed');
    return response((new Mapy_Model_Mapy_Core_Store_Api())->getAvailableCurrencyCodes($storeId, $skipBaseNotAllowed));
})->before($auth);

$app->get('/mapy/core/store/get_base_currency/{storeId}', function ($storeId) {
    return response((new Mapy_Model_Mapy_Core_Store_Api())->getBaseCurrency($storeId));
})->before($auth);

$app->get('/mapy/core/store/get_current_currency/{storeId}', function ($storeId) {
    return response((new Mapy_Model_Mapy_Core_Store_Api())->getCurrentCurrency($storeId));
})->before($auth);
