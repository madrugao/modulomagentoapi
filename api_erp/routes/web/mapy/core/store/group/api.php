<?php
/**
 * Created by PhpStorm.
 * User: heisenberg
 * Date: 4/29/17
 * Time: 6:25 PM
 */

use Symfony\Component\HttpFoundation\Request;

$app->get('/mapy/core/store/group/get_stores/{groupId}', function ($groupId) {
    return response((new Mapy_Model_Mapy_Core_Store_Group_Api())->getStores($groupId));
})->before($auth);

$app->get('/mapy/core/store/group/get_mage_core_model_store_group/{groupId}', function ($groupId) {
    return response((new Mapy_Model_Mapy_Core_Store_Group_Api())->getMageCoreModelStoreGroupAsArray($groupId));
})->before($auth);

$app->delete('/mapy/core/store/group/delete/{groupId}', function ($groupId) {
    return response((new Mapy_Model_Mapy_Core_Store_Group_Api())->delete($groupId));
})->before($auth);

$app->put('/mapy/core/store/group/update/{groupId}', function (Request $request, $groupId) {
    $data = json_decode($request->get('data'), 1);
    return response((new Mapy_Model_Mapy_Core_Store_Group_Api())->update($groupId, $data));
})->before($auth);

$app->get('/mapy/core/store/group/is_can_delete/{groupId}', function ($groupId) {
    return response((new Mapy_Model_Mapy_Core_Store_Group_Api())->isCanDelete($groupId));
})->before($auth);

$app->post('/mapy/core/store/group/create', function (Request $request) {
    $data = json_decode($request->get('data'), 1);
    return response((new Mapy_Model_Mapy_Core_Store_Group_Api())->create($data));
})->before($auth);
