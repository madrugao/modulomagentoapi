<?php
/**
 * Created by PhpStorm.
 * User: heisenberg
 * Date: 4/29/17
 * Time: 6:26 PM
 */

use Symfony\Component\HttpFoundation\Request;

$app->delete('/mapy/core/website/delete/{websiteId}', function ($websiteId) {
    return response((new Mapy_Model_Mapy_Core_Website_Api())->delete($websiteId));
})->before($auth);

$app->put('/mapy/core/website/update/{websiteId}', function (Request $request, $websiteId) {
    $data = json_decode($request->get('data'), 1);
    return response((new Mapy_Model_Mapy_Core_Website_Api())->update($websiteId, $data));
})->before($auth);

$app->post('/mapy/core/website/create', function (Request $request) {
    $data = json_decode($request->get('data'), 1);
    return response((new Mapy_Model_Mapy_Core_Website_Api())->create($data));
})->before($auth);

$app->get('/mapy/core/website/get_store_codes/{websiteId}', function ($websiteId) {
    return response((new Mapy_Model_Mapy_Core_Website_Api())->getStoreCodes($websiteId));
})->before($auth);

$app->get('/mapy/core/website/get_stores/{websiteId}', function ($websiteId) {
    return response((new Mapy_Model_Mapy_Core_Website_Api())->getStores($websiteId));
})->before($auth);

$app->get('/mapy/core/website/get_config/{websiteId}', function (Request $request, $websiteId) {
    $path = $request->get('path');
    return response((new Mapy_Model_Mapy_Core_Website_Api())->getConfig($websiteId, $path));
})->before($auth);

$app->get('/mapy/core/website/is_can_delete/{websiteId}', function ($websiteId) {
    return response((new Mapy_Model_Mapy_Core_Website_Api())->isCanDelete($websiteId));
})->before($auth);

$app->get('/mapy/core/website/get_collection', function () {
    return response((new Mapy_Model_Mapy_Core_Website_Api())->getCollection());
})->before($auth);

$app->get('/mapy/core/website/get_base_currency/{websiteId}', function ($websiteId) {
    return response((new Mapy_Model_Mapy_Core_Website_Api())->getBaseCurrency($websiteId));
})->before($auth);

$app->get('/mapy/core/website/get_mage_core_model_website_as_array/{websiteId}', function ($websiteId) {
    return response((new Mapy_Model_Mapy_Core_Website_Api())->getMageCoreModelWebsiteAsArray($websiteId));
})->before($auth);