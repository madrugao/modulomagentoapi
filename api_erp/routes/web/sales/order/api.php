<?php
/**
 * Created by PhpStorm.
 * User: heisenberg
 * Date: 5/3/17
 * Time: 8:20 PM
 */

use Symfony\Component\HttpFoundation\Request;

$app->get('/sales/order/items', function (Request $request) {
    $filters = json_decode($request->get('filters'), 1);
    return response((new Mapy_Model_Sales_Order_Api_V2())->items($filters));
})->before($auth);

$app->get('/sales/order/items/news', function (Request $request) {
    $filters = json_decode($request->get('filters'), 1);

    return response((new Mapy_Model_Sales_Order_Api_V2())->itemsNew($filters));
})->before($auth);

$app->get('/sales/order/info/{orderIncrementId}', function ($orderIncrementId) {
    return response((new Mapy_Model_Sales_Order_Api_V2())->infoDownload($orderIncrementId));
})->before($auth);

$app->post('/sales/order/add_comment/{orderIncrementId}', function (Response $response, $orderIncrementId) {
    $status = $response->get('status');
    $comment = $response->get('comment');
    $notify = $response->get('notify');
    return response((new Mapy_Model_Sales_Order_Api_V2())->addComment($orderIncrementId, $status, $comment, $notify));
})->before($auth);

$app->put('/sales/order/hold/{orderIncrementId}', function ($orderIncrementId) {
    return response((new Mapy_Model_Sales_Order_Api_V2())->hold($orderIncrementId));
})->before($auth);

$app->put('/sales/order/downloaded/{orderIncrementId}', function ($orderIncrementId) {
    return response((new Mapy_Model_Sales_Order_Api_V2())->downloaded($orderIncrementId));
})->before($auth);

$app->put('/sales/order/unhold/{orderIncrementId}', function ($orderIncrementId) {
    return response((new Mapy_Model_Sales_Order_Api_V2())->unhold($orderIncrementId));
})->before($auth);

$app->put('/sales/order/cancel/{orderIncrementId}', function ($orderIncrementId) {
    return response((new Mapy_Model_Sales_Order_Api_V2())->cancel($orderIncrementId));
})->before($auth);