<?php
/**
 * Created by PhpStorm.
 * User: diego
 * Date: 02/07/17
 * Time: 18:40
 */

use Symfony\Component\HttpFoundation\Request;

$app->get('/sales/order/attributes/items', function (Request $request) {
    $filters = json_decode($request->get('filters'), 1);
    return response((new Mapy_Model_Sales_Order_Shipment_Api_V2())->items($filters));
})->before($auth);
