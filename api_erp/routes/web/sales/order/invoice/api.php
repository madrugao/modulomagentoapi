<?php
/**
 * Created by PhpStorm.
 * User: heisenberg
 * Date: 5/3/17
 * Time: 8:21 PM
 */

use Symfony\Component\HttpFoundation\Request;

$app->get('/sales/order/invoice/items', function (Request $request) {
    $filters = json_decode($request->get('filters'), 1);
    return response((new Mapy_Model_Sales_Order_Invoice_Api_V2())->items($filters));
})->before($auth);

$app->get('/sales/order/invoice/info/{invoiceIncrementId}', function ($invoiceIncrementId) {
    return response((new Mapy_Model_Sales_Order_Invoice_Api_V2())->info($invoiceIncrementId));
})->before($auth);

$app->post('/sales/order/invoice/create/{orderIncrementId}', function (Response $response, $orderIncrementId) {
    $itemsQty = json_decode($response->get('itemsQty'), 1);
    $comment = $response->get('comment');
    $email = $response->get('email');
    $includeComment = $response->get('includeComment');
    return response((new Mapy_Model_Sales_Order_Invoice_Api_V2())->create($orderIncrementId, $itemsQty, $comment, $email, $includeComment));
})->before($auth);

$app->post('/sales/order/invoice/add_comment/{invoiceIncrementId}', function (Response $response, $invoiceIncrementId) {
    $comment = $response->get('comment');
    $email = $response->get('email');
    $includeComment = $response->get('includeComment');
    return response((new Mapy_Model_Sales_Order_Invoice_Api_V2())->addComment($invoiceIncrementId, $comment, $email, $includeComment));
})->before($auth);

$app->put('/sales/order/invoice/capture/{invoiceIncrementId}', function ($invoiceIncrementId) {
    return response((new Mapy_Model_Sales_Order_Invoice_Api_V2())->capture($invoiceIncrementId));
})->before($auth);

$app->put('/sales/order/invoice/void/{invoiceIncrementId}', function ($invoiceIncrementId) {
    return response((new Mapy_Model_Sales_Order_Invoice_Api_V2())->void($invoiceIncrementId));
})->before($auth);

$app->put('/sales/order/invoice/cancel/{invoiceIncrementId}', function ($invoiceIncrementId) {
    return response((new Mapy_Model_Sales_Order_Invoice_Api_V2())->cancel($invoiceIncrementId));
})->before($auth);
