<?php
/**
 * Created by PhpStorm.
 * User: heisenberg
 * Date: 5/3/17
 * Time: 8:21 PM
 */

use Symfony\Component\HttpFoundation\Request;

$app->get('/sales/order/shipment/items', function (Request $request) {
    $filters = $request->get('filters');
    return response((new Mapy_Model_Sales_Order_Shipment_Api_V2())->create($filters));
})->before($auth);

$app->get('/sales/order/shipment/info/{shipmentIncrementId}', function ($shipmentIncrementId) {
    return response((new Mapy_Model_Sales_Order_Shipment_Api_V2())->info($shipmentIncrementId));
})->before($auth);

$app->post('/sales/order/shipment/create/{orderIncrementId}', function (Request $request, $orderIncrementId) {
    $itemsQty = $request->get('itemsQty');
    $comment = $request->get('comment');
    $email = $request->get('email');
    $includeComment = $request->get('includeComment');
    return response((new Mapy_Model_Sales_Order_Shipment_Api_V2())->create($orderIncrementId, $itemsQty, $comment, $email, $includeComment));
})->before($auth);

$app->post('/sales/order/shipment/add_track/{shipmentIncrementId}', function (Request $request, $shipmentIncrementId) {
    $carrier = $request->get('carrier');
    $title = $request->get('title');
    $trackNumber = $request->get('trackNumber');
    return response((new Mapy_Model_Sales_Order_Shipment_Api_V2())->addTrack($shipmentIncrementId, $carrier, $title, $trackNumber));
})->before($auth);

$app->delete('/sales/order/shipment/remove_track/{shipmentIncrementId}/{trackId}', function ($shipmentIncrementId, $trackId) {
    return response((new Mapy_Model_Sales_Order_Shipment_Api_V2())->removeTrack($shipmentIncrementId, $trackId));
})->before($auth);

$app->post('/sales/order/shipment/send_info/{shipmentIncrementId}', function (Request $request, $shipmentIncrementId) {
    $comment = $request->get('comment');
    return response((new Mapy_Model_Sales_Order_Shipment_Api_V2())->sendInfo($shipmentIncrementId, $comment));
})->before($auth);

$app->get('/sales/order/shipment/info_track/{shipmentIncrementId}/{trackId}', function ($shipmentIncrementId, $trackId) {
    return response((new Mapy_Model_Sales_Order_Shipment_Api_V2())->infoTrack($shipmentIncrementId, $trackId));
})->before($auth);

$app->post('/sales/order/shipment/add_comment/{invoiceIncrementId}', function (Response $response, $invoiceIncrementId) {
    $comment = $response->get('comment');
    $email = $response->get('email');
    $includeInEmail = $response->get('includeInEmail');
    return response((new Mapy_Model_Sales_Order_Shipment_Api_V2())->addComment($invoiceIncrementId, $comment, $email, $includeInEmail));
})->before($auth);

$app->get('/sales/order/shipment/get_carriers/{orderIncrementId}', function ($orderIncrementId) {
    return response((new Mapy_Model_Sales_Order_Shipment_Api_V2())->getCarriers($orderIncrementId));
})->before($auth);