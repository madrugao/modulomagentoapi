<?php
class Mapy_Model_Mapy_Cms_Block_Api extends Mage_Api_Model_Resource_Abstract
{

    /**
     *
     * Example API call might look like this $client->call('call', array($session, 'mapy_cms_block.create', array(array('title'=>'My sample block', 'identifier'=>'my-block-id', 'content'=>'Block HTML or plain content', 'is_active'=>'1', 'store_id'=>array('1')))));
     *
     * @param array $data
     * @return int
     */
    public function create(array $data)
    {
        $b = Mage::getModel('cms/block');
        foreach ($data as $k => $v) {
            $b->setData($k, $v);
        }

        try {
            $b->save();
            return $b->getId();
        }
        catch (Exception $e) {
            return 0;
        }

    }


    /**
     *
     * If for some reason you wish to update the CMS block via Mapy API,
     * you can do so by calling this method. Please note that $data is
     * array of key-values matching the data stored in Mage_Cms_Model_Block object.
     * Look in sample $response dump shown under the "public function info($blockId)"
     * to see what kind of data you can set and update.
     *
     * Sample call $client->call('call', array($session, 'mapy_cms_block.update', array(6, array('is_active'=>'0'))));
     *
     * @param integer $blockId
     * @param array $data
     * @return bool
     */
    public function update($blockId, array $data)
    {
        $b = Mage::getModel('cms/block');
        try {
            $b->load($blockId);
            foreach($data as $k => $v) {
                $b->setData($k, $v);
            }
            $b->save();
            return true;
        }
        catch (Exception $e) {
            return false;
        }
        return false;
    }

    /**
     * @param bool $removeContent
     * @return array
     */
    public function getCollection($removeContent = true)
    {
        //Remove 'content' field to reduce the size of response
        $collection = Mage::getModel('cms/block')->getCollection();
        $blocks = array();
        foreach ($collection as $b) {
            $b = $b->toArray();
            if($removeContent) {
                unset($b['content']);
            }
            $blocks[] = $b;
        }
        return $blocks;
    }

    /**
     * @param $blockId
     * @return bool
     */
    public function delete($blockId)
    {
        $b = Mage::getModel('cms/block');
        try {
            $b->load($blockId);
            $b->delete();
            return true;
        } catch (Exception $e) {
            return false;
        }
        return false;
    }

    /**
     * @param $blockId
     * @return array
     */
    public function info($blockId)
    {
        $b = Mage::getModel('cms/block');
        try {
            $b->load($blockId);
            return $b->toArray();
        }
        catch (Exception $e) {
            return array();
        }
    }
}