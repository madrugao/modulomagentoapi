<?php
class Mapy_Model_Mapy_Cms_Page_Api extends Mage_Api_Model_Resource_Abstract
{

    /**
     *
     * Example API call might look like this $client->call('call', array($session, 'mapy_cms_page.create', array(array('title'=>'Sample title', 'identifier'=>'sample-cms', 'store_id'=>array('1'), 'is_active'=>'1', 'root_template'=>'one_column', 'content'=>'Some content goes here'))));
     * Key 'root_template' seems to be the trickiest.
     * @param array $data
     * @return int
     */
    public function create(array $data)
    {
        $p = Mage::getModel('cms/page');
        foreach ($data as $k => $v) {
            $p->setData($k, $v);
        }

        try {
            $p->save();
            return $p->getId();
        }
        catch (Exception $e) {
            return 0;
        }

    }

    /**
     *
     * If for some reason you wish to update the CMS page via Mapy API,
     * you can do so by calling this method. Please note that $data is
     * array of key-values matching the data stored in Mage_Cms_Model_Page object.
     * Look in sample $response dump shown under the "public function info($pageId)"
     * to see what kind of data you can set and update.
     *
     * Sample call $client->call('call', array($session, 'mapy_cms_page.update', array(6, array('is_active'=>'0'))));
     *
     * @param integer $pageId
     * @param array $data
     * @return bool
     */
    public function update($pageId, array $data)
    {
        $p = Mage::getModel('cms/page');
        try {
            $p->load($pageId);
            foreach($data as $k => $v) {
                $p->setData($k, $v);
            }
            $p->save();
            return true;
        }
        catch (Exception $e) {
            return false;
        }
        return false;
    }

    /**
     * @param bool $removeContent
     * @return array
     */
    public function getCollection($removeContent = true)
    {
        //Remove 'content' field to reduce the size of response
        $collection = Mage::getModel('cms/page')->getCollection();
        $pages = array();
        foreach ($collection as $p) {
            $p = $p->toArray();
            if($removeContent) {
                unset($p['content']);
            }
            $pages[] = $p;
        }
        return $pages;
    }

    /**
     * @param $pageId
     * @return bool
     */
    public function delete($pageId)
    {
        $p = Mage::getModel('cms/page');
        try {
            $p->load($pageId);
            $p->delete();
            return true;
        } catch (Exception $e) {
            return false;
        }
        return false;
    }

    /**
     * @param $pageId
     * @return array
     */
    public function info($pageId)
    {
        $p = Mage::getModel('cms/page');
        try {
            $p->load($pageId);
            return $p->toArray();
        }
        catch (Exception $e) {
            return array();
        }
    }
}