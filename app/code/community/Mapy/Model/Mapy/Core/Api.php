<?php
class Mapy_Model_Mapy_Core_Api extends Mage_Api_Model_Resource_Abstract
{
    /**
     *
     * Retrieve the version of Magento.
     * @param string $asType Should be 'array' if we want the method to return array
     * @return mixed Either string or array (if the passed param is 'array')
     */
    public function currentVersion($asType = '')
    {
        if($asType == 'array') {
            return Mage::getVersionInfo();
        }

        return Mage::getVersion();
    }

    /**
     * Retrieve the version of Zend Framework.
     * Returns string like '1.9.6'
     *
     * @return string
     */
    public function currentZendVersion()
    {
        return Zend_Version::VERSION;
    }

    /**
     * Generate url by route and parameters.
     * Basicaly calls for Mage::getUrl($route = '', $params = array());
     *
     * @param $route string
     * @param array $params
     * @return string
     */
    public function getUrl($route = '', $params = array())
    {
        return Mage::getUrl($route, $params);
    }

    /**
     * Retrieve info array of all existing websites
     *
     * @param string $asType
     * @return mixed
     */
    public function getWebsites($asType = 'array')
    {
        switch ($asType) {
            case 'xml':
                return Mage::getModel('core/website')->getCollection()->toXml();
            case 'optionArray':
                return Mage::getModel('core/website')->getCollection()->toOptionArray();
            case 'optionHash':
                return Mage::getModel('core/website')->getCollection()->toOptionHash();
            default:
                return Mage::getModel('core/website')->getCollection()->toArray();
        }
    }

    /**
     * Retrieve info array of all existing stores
     * @param string $asType
     * @return mixed
     */
    public function getStores($asType = 'array')
    {
        switch ($asType) {
            case 'xml':
                return Mage::getModel('core/store')->getCollection()->toXml();
            case 'optionArray':
                return Mage::getModel('core/store')->getCollection()->toOptionArray();
            case 'optionHash':
                return Mage::getModel('core/store')->getCollection()->toOptionHash();
            default:
                return Mage::getModel('core/store')->getCollection()->toArray();
        }
    }


    /**
     * Retrieve config value for store by path
     *
     * @param string $path
     * @param mixed $store
     * @return mixed
     */
    public function getStoreConfig($path, $store = null)
    {
        return Mage::getStoreConfig($path, $store);
    }

    /**
     * Retrieve enabled developer mode
     *
     * @return bool
     */
    public function getIsDeveloperMode()
    {
        return Mage::getIsDeveloperMode();
    }


    /**
     * Mage log facility
     *
     * @param string $message
     * @param integer $level
     * @param string $file
     * @param bool $forceLog
     * @return string Returns string, 'true' if OK, or exception message if exception
     */
    public function log($message, $level = null, $file = '', $forceLog = false)
    {
        try {
            Mage::log($message, $level, $file, $forceLog);
            return 'true';
        }
        catch (Exception $e) {
            return $e->getMessage();
        }

    }
}