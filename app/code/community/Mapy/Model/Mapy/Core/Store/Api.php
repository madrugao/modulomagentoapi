<?php
class Mapy_Model_Mapy_Core_Store_Api extends Mage_Api_Model_Resource_Abstract
{

    /**
     * API call goes like $client->call('call', array($session, 'mapy_core_store.delete', 6));
     *
     * @param indeger $storeId
     * @return bool
     */
    public function delete($storeId)
    {
        $s = Mage::getModel('core/store')->load($storeId);

        try {
            //bypass "Cannot complete this operation from non-admin area."
            Mage::register('isSecureArea', true);
            $s->delete();
            Mage::unregister('isSecureArea');
            return true;
        }
        catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param $storeId
     * @return bool
     */
    public function isCanDelete($storeId)
    {
        $s = Mage::getModel('core/store')->load($storeId);
        try {
            return $s->isCanDelete();
        }
        catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param $storeId
     * @param array $data
     * @return bool
     */
    public function update($storeId, array $data)
    {
        $s = Mage::getModel('core/store')->load($storeId);

        try {
            //bypass "Cannot complete this operation from non-admin area."
            Mage::register('isSecureArea', true);

            foreach ($data as $k => $v) {
                $s->setData($k, $v);
            }

            $s->save();
            Mage::unregister('isSecureArea');
            return true;
        }
        catch (Exception $e) {
            return false;
        }
    }

    /**
     *
     * API call goes like $response = $client->call('call', array($session, 'mapy_core_store.create', array(array('website_id'=>'1', 'group_id'=>'1', 'code'=>'niceee', 'name'=>'My test store'))));
     * @param array $data
     * @return int
     */
    public function create(array $data)
    {
        $s = Mage::getModel('core/store');

        foreach($data as $k => $v) {
            $s->setData($k, $v);
        }

        try {
            $s->save();
            return $s->getId();
        }
        catch (Exception $e) {
            return 0;
        }
    }


    /**
     *
     * Get fully loaded object like array
     * @param integer $storeId
     * @return array
     */
    public function getMageCoreModelStoreAsArray($storeId)
    {
        try {
            $s = Mage::getModel('core/store')->load($storeId);
            return $s->toArray();
        }
        catch (Exception $e) {
            return [];
        }
    }

    /**
     * Get root category id
     *
     * @param $storeId
     * @return int Zero if none
     */
    public function getRootCategoryId($storeId)
    {
        try {
            $s = Mage::getModel('core/store')->load($storeId);

            return $s->getRootCategoryId();
        }
        catch (Exception $e) {
            return 0;
        }
    }

    /**
     * Format price with currency filter (taking rate into consideration)
     *
     * @param   integer $storeId
     * @param   double $price
     * @param   bool $includeContainer
     * @return  string
     */
    public function formatPrice($storeId, $price, $includeContainer = true)
    {
        try {
            $s = Mage::getModel('core/store')->load($storeId);

            return $s->formatPrice($price, $includeContainer);
        }
        catch (Exception $e) {
            return '';
        }
    }


    /**
     * Retrieve store configuration data
     *
     * @param   integer $storeId
     * @param   string $path
     * @return  array
     */
    public function getConfig($storeId, $path)
    {
        try {
            $s = Mage::getModel('core/store')->load($storeId);
            return $s->getConfig($path);
        }
        catch (Exception $e) {
            return array();
        }
    }

    /**
     * Get allowed store currency codes
     *
     * If base currency is not allowed in current website config scope,
     * then it can be disabled with $skipBaseNotAllowed
     *
     * @param integer $storeId
     * @param bool $skipBaseNotAllowed
     * @return array
     */
    public function getAvailableCurrencyCodes($storeId, $skipBaseNotAllowed = false)
    {
        try {
            $s = Mage::getModel('core/store')->load($storeId);
            return $s->getAvailableCurrencyCodes($skipBaseNotAllowed);
        }
        catch (Exception $e) {
            return array();
        }
    }

    /**
     * Retrieve store base currency
     *
     * @param $storeId
     * @return array
     */
    public function getBaseCurrency($storeId)
    {
        try {
            $s = Mage::getModel('core/store')->load($storeId);

            return $s->getBaseCurrency()->toArray();
        }
        catch (Exception $e) {
            return array();
        }
    }

    /**
     * Retrieve store current currency
     *
     * @param $storeId
     * @return array
     */
    public function getCurrentCurrency($storeId)
    {

        try {
            $s = Mage::getModel('core/store')->load($storeId);

            return $s->getCurrentCurrency()->toArray();
        }
        catch (Exception $e) {
            return array();
        }
    }
}