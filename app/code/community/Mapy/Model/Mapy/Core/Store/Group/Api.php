<?php
class Mapy_Model_Mapy_Core_Store_Group_Api extends Mage_Api_Model_Resource_Abstract
{

    /**
     * @param $groupId
     * @return array
     */
    public function getStores($groupId)
    {
        try {
            $g = Mage::getModel('core/store_group')->load($groupId);
            $storesCollection = $g->getStoreCollection();
            $stores = array();

            foreach($storesCollection as $store) {
                $stores[] = $store->debug();
            }
            return $stores;
        }
        catch (Exception $e) {
            return array();
        }
    }


    /**
     *
     * Get fully loaded object like array
     * @param integer $groupId
     * @return array
     */
    public function getMageCoreModelStoreGroupAsArray($groupId)
    {
        try {
            $g = Mage::getModel('core/store_group')->load($groupId);
            return $g->toArray();
        }
        catch (Exception $e) {
            return array();
        }
    }

    /**
     * @param $groupId
     * @return bool
     */
    public function delete($groupId)
    {
        $g = Mage::getModel('core/store_group')->load($groupId);

        try {
            //bypass "Cannot complete this operation from non-admin area."
            Mage::register('isSecureArea', true);
            $g->delete();
            Mage::unregister('isSecureArea');
            return true;
        }
        catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param $groupId
     * @param array $data
     * @return bool
     */
    public function update($groupId, array $data)
    {
        $g = Mage::getModel('core/store_group')->load($groupId);

        try {
            //bypass "Cannot complete this operation from non-admin area."
            Mage::register('isSecureArea', true);

            foreach ($data as $k => $v) {
                $g->setData($k, $v);
            }

            $g->save();
            Mage::unregister('isSecureArea');
            return true;
        }
        catch (Exception $e) {
            return false;
        }
    }


    /**
     * Is can delete group.
     *
     * API call goes like $client->call('call', array($session, 'mapy_core_store_group.isCanDelete', 3));
     *
     * @param $groupId
     * @return bool
     */
    public function isCanDelete($groupId)
    {
        $g = Mage::getModel('core/store_group')->load($groupId);
        try {
            return $g->isCanDelete();
        }
        catch (Exception $e) {
            return false;
        }
    }


    /**
     *
     * API call goes like $response = $client->call('call', array($session, 'mapy_core_store.create', array(array('website_id'=>'1', 'group_id'=>'1', 'code'=>'niceee', 'name'=>'My test store'))));
     * @param array $data
     * @return int
     */
    public function create(array $data)
    {
        $g = Mage::getModel('core/store_group');

        foreach ($data as $k => $v) {
            $g->setData($k, $v);
        }
        try {
            $g->save();
            return $g->getId();
        }
        catch (Exception $e) {
            return 0;
        }
    }

}