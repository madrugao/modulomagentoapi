<?php
class Mapy_Model_Mapy_Core_Website_Api extends Mage_Api_Model_Resource_Abstract
{
    /**
     * API call goes like $client->call('call', array($session, 'mapy_core_website.delete', 2));
     *
     * @param $websiteId
     * @return bool
     * @internal param indeger $storeId
     */
    public function delete($websiteId)
    {
        $w = Mage::getModel('core/website')->load($websiteId);

        try {
            //bypass "Cannot complete this operation from non-admin area."
            Mage::register('isSecureArea', true);
            $w->delete();
            Mage::unregister('isSecureArea');
            return true;
        }
        catch (Exception $e) {
            return false;
        }
    }


    /**
     * @param $websiteId
     * @param array $data
     * @return bool
     */
    public function update($websiteId, array $data)
    {
        $w = Mage::getModel('core/website')->load($websiteId);

        try {
            //bypass "Cannot complete this operation from non-admin area."
            Mage::register('isSecureArea', true);

            foreach ($data as $k => $v) {
                $w->setData($k, $v);
            }

            $w->save();
            Mage::unregister('isSecureArea');
            return true;
        }
        catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param array $data
     * @return int
     */
    public function create(array $data)
    {
        $w = Mage::getModel('core/website');

        foreach($data as $k => $v) {
            $w->setData($k, $v);
        }

        try {
            $w->save();
            return $w->getId();
        }
        catch (Exception $e) {
            return 0;
        }
    }


    /**
     * Retrieve website store codes
     *
     * @param $websiteId
     * @return array
     */
    public function getStoreCodes($websiteId)
    {
        try {
            $w = Mage::getModel('core/website')->load($websiteId);
            if($w->getId()) {
                /**
                $response array(3) {
                [1] => string(7) "default"
                [3] => string(6) "french"
                [2] => string(6) "german"
                }
                 */
                return $w->getStoreCodes();
            }
        }
        catch (Exception $e) {
            return [];
        }
    }

    /**
     * Retrieve website store list
     *
     * @param $websiteId
     * @return array
     */
    public function getStores($websiteId)
    {
        try {
            $w = Mage::getModel('core/website')->load($websiteId);

            $storesCollection = $w->getStores();
            $stores = [];

            foreach($storesCollection as $store) {
                $store = Mage::getModel('core/store')->load($store->getId());
                $stores[] = $store->toArray();
                unset($store);
            }
            return $stores;
        }
        catch (Exception $e) {
            return [];
        }
    }

    /**
     * Get website config data
     *
     * @param $websiteId
     * @param string $path
     * @return array
     */
    public function getConfig($websiteId, $path)
    {
        try {
            $w = Mage::getModel('core/website')->load($websiteId);
            return $w->getConfig($path);
        }
        catch (Exception $e) {
            return [];
        }
    }


    /**
     * is can delete website
     *
     * @param $websiteId
     * @return bool
     */
    public function isCanDelete($websiteId)
    {
        try {
            $w = Mage::getModel('core/website')->load($websiteId);
            return $w->isCanDelete();
        }
        catch (Exception $e) {
            return false;
        }
    }

    /**
     * Get list of all websites
     *
     * @return array
     */
    public function getCollection()
    {
        $websites = [];

        $collection = Mage::getModel('core/website')->getCollection();

        foreach($collection as $website) {
            //Load full object data
            $websites[] = $website->toArray();
        }
        return $websites;
    }

    /**
     * Retrieve website base currency
     *
     * @param $websiteId
     * @return array
     */
    public function getBaseCurrency($websiteId)
    {
        try {
            $w = Mage::getModel('core/website')->load($websiteId);
            return $w->getBaseCurrency()->toArray();
        }
        catch (Exception $e) {
            return [];
        }
    }

    /**
     *
     * Get fully loaded object like array
     * @param integer $websiteId
     * @return array
     */
    public function getMageCoreModelWebsiteAsArray($websiteId)
    {
        try {
            $w = Mage::getModel('core/website')->load($websiteId);
            return $w->toArray();
        }
        catch (Exception $e) {
            return [];
        }
    }
}