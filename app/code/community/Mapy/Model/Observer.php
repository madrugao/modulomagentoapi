<?php
class Mapy_Model_Observer
{
    public function downOrder(Varien_Event_Observer $observer){
        $order = $observer->getEvent()->getOrder();

        $orderModel = Mage::getModel('sales/order')->loadByIncrementId($order->getIncrementId());
        $orderModel->setData('mapy_downloaded_at',date('Y-m-d H:i:s'))->save();
    }
}