<?php

class Mapy_Model_Sales_Order_Api_V2 extends Mage_Sales_Model_Order_Api_V2
{

    public function itemsNew($filters)
    {
        $orders = array();

        /** @var $orderCollection Mage_Sales_Model_Mysql4_Order_Collection */
        $orderCollection = Mage::getModel("sales/order")->getCollection();
        $orderCollection
            ->getSelect()
            ->limit(20);
        $orderCollection
            ->addAttributeToSelect('increment_id')
            ->addAttributeToFilter('mapy_reported_at', ['lteq' => date('Y-m-d H:i:s', strtotime('-1 hour')), 'null' => true])
            ->addAttributeToFilter('created_at', ['gteq' => '2017-08-22 00:00:00'])
            ->addAttributeToFilter('mapy_downloaded_at', [['lt' => new Zend_Db_Expr('updated_at')]  , ['null' => true]]);

        /** @var $apiHelper Mage_Api_Helper_Data */
        $apiHelper = Mage::helper('api');
        $filters = $apiHelper->parseFilters($filters, $this->_attributesMap['order']);
        try {
            foreach ($filters as $field => $value) {
                $orderCollection->addFieldToFilter($field, $value);
            }
        } catch (Mage_Core_Exception $e) {
            $this->_fault('filters_invalid', $e->getMessage());
        }
        foreach ($orderCollection as $order) {
            $orderModel = Mage::getModel('sales/order')->loadByIncrementId($order->getIncrementId());
            $orderModel->setData('mapy_reported_at',date('Y-m-d H:i:s'))->save();
            $orders[] = $this->_getAttributes($order, 'order');
        }
        return $orders;
    }

    public function infoDownload($orderIncrementId)
    {
        // TODO Jorge, edita aqui pra trocar as informações
        return $this->info($orderIncrementId);
    }
    /**
     * @param $orderIncrementId
     */
    public function downloaded($orderIncrementId)
    {
        $order = $this->_initOrder($orderIncrementId);
        return $order->setData('mapy_downloaded_at',date('Y-m-d H:i:s'))->save();
    }
}
